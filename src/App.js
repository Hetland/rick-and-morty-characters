import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';

import AllCharacters from './views/components/allCharacters.js';
import CharacterDetails from './views/components/characterDetails.js';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      characters: [],
      nextCharactersToFetch: 0,
      allCharactersUrl: "https://rickandmortyapi.com/api/character/",
      charId: null
    }
  }

  componentDidMount() {
    this.fetchSomeChars();
  }

  range = (size, startAt) => {
    return [...Array(size).keys()].map(i => i + startAt);
  }

  getAllChars = () => {
    return this.state.characters;
  }

  // fetchOriginalChars = () => {
  //   const charsToFetchArray = this.range(20, this.state.nextCharactersToFetch);
  //   const charsToFetchString = charsToFetchArray.reduce((total, next) => total + "," +next);
  //   fetch(this.state.allCharactersUrl + charsToFetchString)
  //   .then(response => response.json())
  //   .then(json => {
  //     if (json.error) { return }
  //     const startingPointNextCharacters = this.state.nextCharactersToFetch + 20;
  //     this.setState({ characters: json,
  //                     nextCharactersToFetch: startingPointNextCharacters })
  //   })
  //   .catch(error => console.log(error));
  // }

  fetchSomeChars = () => {
    const charsToFetchArray = this.range(20, this.state.nextCharactersToFetch);
    const charsToFetchString = charsToFetchArray.reduce((total, next) => total + "," + next);
    fetch(this.state.allCharactersUrl + charsToFetchString)
    .then(response => response.json())
    .then(json => {
      if (json.error) { return }
      const startingPointNextCharacters = this.state.nextCharactersToFetch + 20;
      this.setState({ characters: this.state.characters.concat(json),
                      nextCharactersToFetch: startingPointNextCharacters })
    })
    .catch(error => console.log(error));
  }

  render() {
    return (
      <div className="App">
        <header className="header-for-app"></header>
        <div className="all-routes">
          <Switch>
            <Route path={"/details/:charId"} render={() => <CharacterDetails /> }/>
            <Route path="/" render={() => <AllCharacters characters={this.state.characters} onFetchMoreChars={this.fetchSomeChars} /> } />
          </Switch>
        </div>
      </div>
    );  
  }
}

export default App;
