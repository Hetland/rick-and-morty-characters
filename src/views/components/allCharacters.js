import React from 'react';
import CharacterCard from './characterCard';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link
// } from "react-router-dom";

class AllCharacters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      characters: []
    }
  }

  render() {
    const fetchMoreButtonStyle = {  width: '200px', 
                                    height: '60px', 
                                    marginBottom: '50px',
                                    borderColor: 'rgb(175,205,202)',
                                    backgroundColor: 'rgb(175,205,202)', 

    };
    return (
      <div className="all-encompassing">
        <div className="all-characters">
          {this.props.characters.map(character => {
            return <CharacterCard key={character.id} character={character} />;
          })}
        </div>
        <div>
        <button style={fetchMoreButtonStyle} 
                onClick={this.props.onFetchMoreChars}>
                Load more
        </button>
        </div>
      </div>

    );
  }
}


export default AllCharacters;
