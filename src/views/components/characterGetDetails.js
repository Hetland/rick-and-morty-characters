import React from 'react';
import CharacterDisplayDetails from './characterDisplayDetails';
import default_char from '../../images/default_char.jpeg';
//import { Link } from 'react-router-dom';

class CharacterGetDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      characterDetails: {
        "name":"Unknown",
        "status":"Unknown",
        "species":"Unknown",
        "type":"Unknown",
        "gender":"Unknown",
        "origin":{"name":"Unknown"},
        "location":{"name":"Unknown"},
        "image": default_char,
      }
    }
  }

  getCharDetails = () => {
    const charToGetUrl = "https://rickandmortyapi.com/api/character/" + this.props.charId;
    fetch( charToGetUrl )
    .then( (response) => { return response.json(); })
    .then( (json) => { 
      if (json.error) { return }
      this.setState({ characterDetails: json }); })
    .catch( error => console.log(error) );
  }

  componentDidMount() {
    this.getCharDetails();
  }

  render() {
    return (
      <div className="character-get-details">
        <CharacterDisplayDetails details={this.state.characterDetails}/>
      </div>
    );
  }
}

export default CharacterGetDetails;
