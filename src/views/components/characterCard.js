import React from 'react';
import { Link } from 'react-router-dom';
import {ListGroup, 
        ListGroupItem
    } from 'react-bootstrap';


class CharacterCard extends React.Component {

  render() {
    const linkStyle = {color: 'black', textDecoration: 'none'};
    const imgStyle = {height: '200px', width: '200px'};
    return (
      <div className="character-card">
        <Link to={"/details/"+this.props.character.id} style={linkStyle}>
          <img src={this.props.character.image} alt='Character' style={imgStyle}></img>
          <h1>{this.props.character.name}</h1>
          <ListGroup className="list-group-flush">
            <ListGroupItem>Status: {this.props.character.status}</ListGroupItem>
            <ListGroupItem>Species: {this.props.character.species}</ListGroupItem>
            <ListGroupItem>Gender: {this.props.character.gender}</ListGroupItem>
            <ListGroupItem>Origin: {this.props.character.origin.name}</ListGroupItem>
            <ListGroupItem>Last location: {this.props.character.location.name}</ListGroupItem>
          </ListGroup>
        </Link>
      </div>
    );
  }
}

export default CharacterCard;
