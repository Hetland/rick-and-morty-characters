import React from 'react';
import Card from 'react-bootstrap/Card';
import {ListGroup, 
        ListGroupItem
    } from 'react-bootstrap';

class CharacterDisplayDetails extends React.Component {

    render() {
        return (
            <div className="character-display-details">
                <Card id='card' style={{ width: '25rem' }}>
                    <Card.Img variant="top" src={this.props.details.image} />
                    <Card.Body>
                            <Card.Title><h1>{this.props.details.name}</h1></Card.Title>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                        <h1>Profile</h1>
                        <ListGroupItem>Status: {this.props.details.status}</ListGroupItem>
                        <ListGroupItem>Species: {this.props.details.species}</ListGroupItem>
                        <ListGroupItem>Gender: {this.props.details.gender}</ListGroupItem>
                        <ListGroupItem>Origin: {this.props.details.origin.name}</ListGroupItem>
                        <h1>Location</h1>
                        <ListGroupItem>Current location: {this.props.details.location.name}</ListGroupItem>
                    </ListGroup>
                </Card>
            </div>
          );
    }
}
  
export default CharacterDisplayDetails;
