import React from 'react';
import { useParams } from 'react-router-dom';
import CharacterGetDetails from './characterGetDetails';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link
// } from "react-router-dom";

const CharacterDetails = () => {
  let { charId } = useParams();

    return (
      <div className="character-details">
        <CharacterGetDetails charId={charId} />
      </div>
    );
  }

export default CharacterDetails;
