FROM node:lts-alpine AS builder
ENV PORT 3000
WORKDIR /src
COPY package.json .
RUN npm install
RUN npm install express morgan
COPY . .
RUN npm run build
EXPOSE 3000/tcp
CMD ["node", "App.js"]